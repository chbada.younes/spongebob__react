import React from 'react'
import VideoItem from './VideoItem'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  background-color: #C7DCB9;
`;


const VideoList = ({ videos, onVideoSelect }) => {
    const ListOfVideos = videos.map((video, id) => <VideoItem key= {id} onVideoSelect={onVideoSelect} video={video} />) 
    return (
        <>
        <Wrapper>
            {ListOfVideos}
        </Wrapper>
        </>
    )
}

export default VideoList;