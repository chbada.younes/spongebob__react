import styled from 'styled-components'

export const NavbarSection = styled.div`
    width:100%;
    text-align:center;
    padding: 5px 0;
    overflow: hidden;
    background: #fff;
    position: relative;
`

export const LogoLeft = styled.div`
    float: left;
    padding: 10px 20px;
    &:hover {
        transform: scale(1.1);
    }
`

export const LogoRight = styled.div`
    float: right;
    text-align:center;
    padding: 10px;
    &:hover {
        transform: scale(1.1);
    }
`

export const LogoText = styled.h2 `
    font-size: 12px;
    color: #666;
    font-weight: bold;
    float: right;
`

export const UlList = styled.ul`
    width: 50%;
    float: left;
    list-style: none;
    padding: 0;
`

export const ListItem = styled.li`
    display: inline-block;
`

export const Anchor = styled.a`
    display:block;
    color: #222;
    text-decoration: none;
    padding: 10px 15px;
    font-weight: bold;

    &:hover {
        color: #eb5424;
    }
`
