import React from 'react'
import nickelodeon from "../../images/Nickelodeon.png"
import nickelodeonTeen from "../../images/NickelodeonTeen.png"
import { NavbarSection, LogoRight,LogoLeft, LogoText } from './style'
export { default as SearchBar } from '../SearchBar'

const Header = () => {
    return (
        <NavbarSection className="navbar">
            <div className="container">
                <LogoLeft className="logo">
                        <img src={nickelodeon} alt="Nickelodeon" />
                </LogoLeft>
                <LogoRight>
                    <LogoText>Plus</LogoText>
                </LogoRight>
                <LogoRight>
                    <img src={nickelodeonTeen} alt="NickelodeonTeen"/>
                </LogoRight>
            </div>
        </NavbarSection>
    )
}

export default Header
