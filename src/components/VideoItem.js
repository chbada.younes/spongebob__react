import React from 'react';
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 250px;
  height: 250px;
  background-color: #c4b2a9;
  border-radius: 4px;
  padding: 1rem;
  margin: 1rem;
  overflow: hidden;

  &:hover {
    opacity: 0.5;
    cursor: pointer;
    animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both;
    transform: translate3d(0, 0, 0);
    backface-visibility: hidden;
    perspective: 1000px;
    
    @keyframes shake {
    10%, 90% {
        transform: translate3d(-1px, 0, 0);
    }
    
    20%, 80% {
        transform: translate3d(2px, 0, 0);
    }

    30%, 50%, 70% {
        transform: translate3d(-4px, 0, 0);
    }

    40%, 60% {
        transform: translate3d(4px, 0, 0);
    }
  }
}
  
`;

const Description = styled.p`
  color: white;
  text-align: center;
`;


const VideoItem = ( {video , onVideoSelect}) => {
    return(
        <Wrapper  onClick={() => onVideoSelect(video)}>
            <img alt="thumbnail" src={video.snippet.thumbnails?.medium?.url}></img>
            <Description>{video.snippet.description}</Description>
        </Wrapper>
    )
}

export default VideoItem;