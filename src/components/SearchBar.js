import React, { useState } from 'react'
import styled from 'styled-components'



const Form = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
  background-color: #6F00AF;
  /* Change width of the form depending if the bar is opened or not */
  width: ${props => (props.barOpened ? "20rem" : "1rem")};
  /* If bar opened, normal cursor on the whole form. If closed, show pointer on the whole form so user knows he can click to open it */
  cursor: ${props => (props.barOpened ? "auto" : "pointer")};
  padding: 1.1rem;
  height: 1.1rem;
  border-radius: 10rem;
  transition: width 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
`;

const Input = styled.input`
  font-size: 14px;
  line-height: 1;
  background-color: transparent;
  width: 100%;
  margin-left: ${props => (props.barOpened ? "1rem" : "0rem")};
  border: none;
  color: white;
  transition: margin 300ms cubic-bezier(0.645, 0.045, 0.355, 1);

  &:focus,
  &:active {
    outline: none;
  }
  &::placeholder {
    color: white;
  }
  line-height: 1;
  pointer-events: ${props => (props.barOpened ? "auto" : "none")};
  cursor: ${props => (props.barOpened ? "pointer" : "none")};
  background-color: transparent;
  border: none;
  outline: none;
  color: white;
`;

const Button = styled.button`
  line-height: 1;
  pointer-events: ${props => (props.barOpened ? "auto" : "none")};
  cursor: ${props => (props.barOpened ? "pointer" : "none")};
  background-color: transparent;
  border: none;
  outline: none;
  color: white;
`;

const SearchBarDiv = styled.div`
font-family: sans-serif;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: -20px;
  /* height: 100%; */
  /* min-height: 100vh; */
`

const SearchBar = ({onFormSubmit}) => {
    const [barOpened, setBarOpened] = useState(false);
    const [ searchTerm, setSearchTerm ] = useState("Bob l'éponge");
    
    const handleChange = (event) => setSearchTerm(event.target.value);

    const onKeyPress = (event) => {
        if(event.key === 'Enter') {
            onFormSubmit(searchTerm)
        }
    }
  
    return (
        <SearchBarDiv>
            <Form
            barOpened={barOpened}
            onClick={() => {
                // When form clicked, set state of baropened to true and focus the input
                setBarOpened(true);
            }}
            // on focus open search bar
            onFocus={() => {
                setBarOpened(true);
            }}
            // on blur close search bar
            onBlur={() => {
                setBarOpened(false);
            }}
            >
            <Button barOpened={barOpened}>
            </Button>
            <Input
                value={searchTerm}
                onChange={handleChange}
                onKeyPress={onKeyPress}
                barOpened={barOpened}
                placeholder="Search for videos..."
            />
            </Form>
        </SearchBarDiv>
    )
}

export default SearchBar;