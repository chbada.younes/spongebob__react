import React, { useState } from 'react';
import { Header, SearchBar, VideoDetail, VideoList } from './components'; 
import youtube from './api/youtube';
import CssBaseline from '@material-ui/core/CssBaseline'
import { Grid, Row, Col } from './Grid'
import styled from 'styled-components'
import banner from './images/banner.png'

const App = () => {
    const [ videos, setVideos ] = useState([]);
    const [ selectedVideo, setSelectedVideo ] = useState(null); 


    const handleSubmit =  async (searchTerm) => { 
        const response = await youtube.get('search', { 
            params: {
                part: 'snippet',
                maxResults: 10,
                key: 'AIzaSyCQcGMGZibpHy_B-j1xL3t5EUXHr0Vp_Dg',
                q: searchTerm,
            }
        });
        
        setVideos(response.data.items)
        setSelectedVideo(response.data.items[0])
    }
    
    const Banner = styled.img`
    min-height: 100%;
    width: 100%;
    height: 100%;
    margin-bottom: -5px;
`
    return (
        <div className="App">
            <CssBaseline/>
            <Grid>
            <Row>
                <Col  size={1}>
                    <Header></Header>
                    <SearchBar onFormSubmit={ handleSubmit }/>
                    <div>
                        <Banner src={banner} alt="Banner"/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Row>
                    <Col size={10}>
                        <VideoDetail video={selectedVideo}/>
                    </Col>
                    <Col size={2}>
                        <VideoList videos={videos} onVideoSelect={setSelectedVideo}></VideoList>
                    </Col>
                </Row>
            </Row>
        </Grid>
        </div>
    )
}

export default App;